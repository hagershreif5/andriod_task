package com.example.task;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    EditText name;
    Button ok;
    String inputName;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        name=findViewById(R.id.name);
        ok=findViewById(R.id.Ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                opentToaast();
            }
        });
    }
    public void opentToaast(){
        Intent myIntent=new Intent(this,toaast.class);
        startActivity(myIntent);
    }
    @Override
    protected void onPause() {
        super.onPause();
        inputName=name.getText().toString();
    Toast.makeText(this,inputName,Toast.LENGTH_LONG).show();
    }
}
